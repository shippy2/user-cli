package main

import (
	"context"
	"log"
	"os"

	pb "user-cli/proto/user"

	"github.com/micro/cli"
	"github.com/micro/go-micro"
	mc "github.com/micro/go-micro/client"
	"github.com/micro/go-micro/config/cmd"
)

func main() {
	cmd.Init()

	client := pb.NewUserService("user", mc.DefaultClient)

	service := micro.NewService(

		micro.Flags(
			cli.StringFlag{
				Name:  "name",
				Usage: "Your full name",
			},
			cli.StringFlag{
				Name:  "email",
				Usage: "Your email",
			},
			cli.StringFlag{
				Name:  "password",
				Usage: "Your password",
			},
			cli.StringFlag{
				Name:  "company",
				Usage: "Your company",
			},
		),
	)

	service.Init(micro.Action(func(c *cli.Context) {
		name := c.String("name")
		email := c.String("email")
		passwd := c.String("password")
		company := c.String("company")

		r, err := client.Create(context.TODO(), &pb.User{
			Name:     name,
			Email:    email,
			Password: passwd,
			Company:  company,
		})

		if err != nil {
			log.Fatalf("Could not create: %v", err)
		}
		log.Printf("Created: %s", r.User.Id)

		all, err := client.GetAll(context.Background(), &pb.Requst{})
		if err != nil {
			log.Fatalf("Could not list users: %v", err)
		}

		for _, v := range all.Users {
			log.Println(v)
		}

		os.Exit(0)

	}))
}
